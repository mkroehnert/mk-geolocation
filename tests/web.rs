//! Test suite for the Web and headless browsers.

#![cfg(all(target_arch = "wasm32", feature = "futures"))]

use futures_channel::{mpsc, oneshot};
