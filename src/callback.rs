//! Callback style geolocation API

use crate::geolocation::{
    Geolocation, GeolocationPosition, GeolocationPositionError, PositionOptions,
};
use wasm_bindgen::prelude::*;
use wasm_bindgen::{JsCast, JsValue};
use web_sys::Window;

pub fn position_options(
    high_accuracy_enabled: bool,
    max_age_ms: u32,
    timeout_ms: u32,
) -> PositionOptions {
    let mut options = PositionOptions::new();
    options.enable_high_accuracy(high_accuracy_enabled);
    options.maximum_age(max_age_ms);
    options.timeout(timeout_ms);
    options
}

pub fn position_options_default() -> PositionOptions {
    let mut options = PositionOptions::new();
    options.enable_high_accuracy(false);
    options.maximum_age(7000);
    options.timeout(6500);
    options
}

/// struct for storing an instance of window.navigator.geolocation
struct MkGeolocation {
    geolocation: Geolocation,
}

thread_local! {
    /// Thread local static instance of the JS geolocation object.
    static GLOBAL: MkGeolocation = MkGeolocation::new();
}

impl MkGeolocation {
    /// create new instance of MkGeolocation
    fn new() -> Self {
        #[wasm_bindgen]
        extern "C" {
            type Global;

            #[wasm_bindgen(method, getter, js_name = Window)]
            fn window(this: &Global) -> JsValue;
        }

        let global: Global = js_sys::global().unchecked_into();

        if !global.window().is_undefined() {
            let navigator = global
                .unchecked_into::<Window>()
                .navigator()
                .unchecked_into::<crate::geolocation::Navigator>();
            let geolocation = navigator.geolocation().unwrap_throw();
            Self { geolocation }
        } else {
            panic!("Only supported in a browser or web worker");
        }
    }
}

/// Store callbacks used in the one_shot request.
#[derive(Debug)]
#[must_use = "Geolocation cancel on drop; either call `forget` or `drop` explicitly"]
pub struct Position {
    success_closure: Option<Closure<dyn FnMut(GeolocationPosition)>>,
    error_closure: Option<Closure<dyn FnMut(GeolocationPositionError)>>,
}

impl std::ops::Drop for Position {
    /// cancel request when object is destroyed
    fn drop(&mut self) {
        // nothing to cancel for one_shot request
    }
}

impl Position {
    pub fn new<F>(success_callback: F) -> Self
    where
        F: 'static + FnOnce(GeolocationPosition),
    {
        // TODO check
        // https://docs.rs/wasm-bindgen/0.2.58/wasm_bindgen/closure/struct.Closure.html
        // once_into_js() -> without cancellation
        let success_closure = Closure::once(success_callback);
        GLOBAL.with(|global| {
            global
                .geolocation
                .get_current_position(success_closure.as_ref().unchecked_ref())
        });
        Position {
            success_closure: Some(success_closure),
            error_closure: None,
        }
    }

    pub fn new_with_error_callback<F, G>(success_callback: F, error_callback: G) -> Self
    where
        F: 'static + FnOnce(GeolocationPosition),
        G: 'static + FnOnce(GeolocationPositionError),
    {
        let success_closure = Closure::once(success_callback);
        let error_closure = Closure::once(error_callback);
        GLOBAL.with(|global| {
            global.geolocation.get_current_position_with_error_callback(
                success_closure.as_ref().unchecked_ref(),
                Some(error_closure.as_ref().unchecked_ref()),
            )
        });
        Position {
            success_closure: Some(success_closure),
            error_closure: Some(error_closure),
        }
    }

    pub fn new_with_error_callback_and_options<F, G>(
        success_callback: F,
        error_callback: G,
        options: PositionOptions,
    ) -> Self
    where
        F: 'static + FnOnce(GeolocationPosition),
        G: 'static + FnOnce(GeolocationPositionError),
    {
        let success_closure = Closure::once(success_callback);
        let error_closure = Closure::once(error_callback);
        GLOBAL.with(|global| {
            global
                .geolocation
                .get_current_position_with_error_callback_and_options(
                    success_closure.as_ref().unchecked_ref(),
                    Some(error_closure.as_ref().unchecked_ref()),
                    &options,
                )
        });
        Position {
            success_closure: Some(success_closure),
            error_closure: Some(error_closure),
        }
    }

    pub fn forget(mut self) {
        self.success_closure.take().unwrap_throw().forget();
        self.error_closure.take().unwrap_throw().forget();
    }

    /// Cancel this one_shot position request, so that the callbacks are not invoked
    /// after the time is up.
    ///
    /// The scheduled callbacks are returned.
    ///
    /// # Example
    ///
    /// ```no_run
    /// use mk_geolocation::callback::Position;
    ///
    /// let position = Position::new(move |position: mk_geolocation::GeolocationPosition| {
    ///     // use position...
    /// });
    ///
    /// // If position shouldn't be watched, then cancel it.
    /// if nevermind() {
    ///     position.cancel();
    /// }
    /// # fn nevermind() -> bool { true }
    /// ```
    pub fn cancel(
        mut self,
    ) -> (
        Closure<dyn FnMut(GeolocationPosition)>,
        Option<Closure<dyn FnMut(GeolocationPositionError)>>,
    ) {
        let success = self.success_closure.take().unwrap_throw();
        let error = self.error_closure.take();
        (success, error)
    }
}

/// Store callbacks and the ID required to cancel the continuous position request.
#[derive(Debug)]
#[must_use = "Geolocation cancel on drop; either call `forget` or `drop` explicitly"]
pub struct ContinuousPosition {
    watch_id: Option<i32>,
    success_closure: Option<Closure<dyn FnMut(GeolocationPosition)>>,
    error_closure: Option<Closure<dyn FnMut(GeolocationPositionError)>>,
}

impl std::ops::Drop for ContinuousPosition {
    /// Clear the continuous position request when the object is dropped
    /// or when `drop` is called manually.
    fn drop(&mut self) {
        if let Some(watch_id) = self.watch_id {
            GLOBAL.with(|global| global.geolocation.clear_watch(watch_id));
        }
    }
}

impl ContinuousPosition {
    /// Schedule continuous position requests.
    /// `success_callback` is called everytime a new position is returned.
    ///
    /// # Example
    ///
    /// ```no_run
    /// use mk_geolocation::callback::ContinuousPosition;
    ///
    /// let continuous_pos = ContinuousPosition::new(move |position: mk_geolocation::GeolocationPosition| {
    ///     // use position ...
    /// });
    ///
    /// continuous_pos.forget();
    /// ```
    pub fn new<F>(success_callback: F) -> Self
    where
        // TODO: Fn or FnMut?
        F: 'static + FnMut(GeolocationPosition),
    {
        let success_closure =
            Closure::wrap(Box::new(success_callback) as Box<dyn FnMut(GeolocationPosition)>);
        let watch_id = GLOBAL.with(|global| {
            global
                .geolocation
                .watch_position(success_closure.as_ref().unchecked_ref())
        });
        Self {
            watch_id: Some(watch_id),
            success_closure: Some(success_closure),
            error_closure: None,
        }
    }

    /// Schedule continuous position requests.
    /// `success_callback` is called everytime a new position is returned.
    /// `error_callback` is called everytime a position error is encountered.
    ///
    /// # Example
    ///
    /// ```no_run
    /// use mk_geolocation::callback::ContinuousPosition;
    ///
    /// lcontinuous_pos = ContinuousPosition::new(move |position: mk_geolocation::GeolocationPosition| {
    ///     // use position ...
    /// }, move |position_error: mk_geolocation::GeolocationPositionError| {
    ///     // use position_error ...
    /// });
    ///
    /// continuous_pos.forget();
    /// ```
    pub fn new_with_error_callback<F, G>(success_callback: F, error_callback: G) -> Self
    where
        F: 'static + FnMut(GeolocationPosition),
        G: 'static + FnMut(GeolocationPositionError),
    {
        let success_closure =
            Closure::wrap(Box::new(success_callback) as Box<dyn FnMut(GeolocationPosition)>);
        let error_closure =
            Closure::wrap(Box::new(error_callback) as Box<dyn FnMut(GeolocationPositionError)>);
        let watch_id = GLOBAL.with(|global| {
            global.geolocation.watch_position_with_error_callback(
                success_closure.as_ref().unchecked_ref(),
                Some(error_closure.as_ref().unchecked_ref()),
            )
        });
        Self {
            watch_id: Some(watch_id),
            success_closure: Some(success_closure),
            error_closure: Some(error_closure),
        }
    }

    /// Schedule continuous position requests parameterized by `options`.
    /// `success_callback` is called everytime a new position is returned.
    /// `error_callback` is called everytime a position error is encountered.
    ///
    /// # Example
    ///
    /// ```no_run
    /// use mk_geolocation::callback::ContinuousPosition;
    ///
    /// // high_accuracy_enabled/max_age_ms/timeout_ms
    /// let options = position_options(true, 6000, 2000);
    ///
    /// let continuous_pos = ContinuousPosition::new(move |position: mk_geolocation::GeolocationPosition| {
    ///     // use position ...
    /// }, move |position_error: mk_geolocation::GeolocationPositionError| {
    ///     // use position_error ...
    /// }, &options);
    ///
    /// continuous_pos.forget();
    /// ```
    pub fn new_with_error_callback_and_options<F, G>(
        success_callback: F,
        error_callback: G,
        options: PositionOptions,
    ) -> Self
    where
        F: 'static + FnMut(GeolocationPosition),
        G: 'static + FnMut(GeolocationPositionError),
    {
        let success_closure =
            Closure::wrap(Box::new(success_callback) as Box<dyn FnMut(GeolocationPosition)>);
        let error_closure =
            Closure::wrap(Box::new(error_callback) as Box<dyn FnMut(GeolocationPositionError)>);
        let watch_id = GLOBAL.with(|global| {
            global
                .geolocation
                .watch_position_with_error_callback_and_options(
                    success_closure.as_ref().unchecked_ref(),
                    Some(error_closure.as_ref().unchecked_ref()),
                    &options,
                )
        });
        Self {
            watch_id: Some(watch_id),
            success_closure: Some(success_closure),
            error_closure: Some(error_closure),
        }
    }

    /// Make this continuous position request uncancel-able.
    ///
    /// Returns the identifier returned by the original `watch_position` call, and
    /// therefore you can still cancel the timeout by calling `clearWatch`
    /// directly (perhaps via `web_sys::clear_timeout_with_handle`).
    ///
    /// # Example
    ///
    /// ```no_run
    /// use mk_geolocation::callback::ContinuousPosition;
    ///
    /// // We definitely want to get and use a position, and aren't going to ever cancel this
    /// // request.
    /// let watch_id = ContinuousPosition::new(move |position: mk_geolocation::GeolocationPosition| {
    ///     // use position...
    /// }).forget();
    /// ```
    pub fn forget(mut self) -> i32 {
        // id must be cleared first, for the case that closure::forget() fails
        let watch_id = self.watch_id.take().unwrap_throw();
        self.success_closure.take().unwrap_throw().forget();
        self.error_closure.take().unwrap_throw().forget();
        watch_id
    }

    /// Cancel this continuous position request, so that the callbacks are not invoked
    /// after the time is up.
    ///
    /// The scheduled callbacks are returned.
    ///
    /// # Example
    ///
    /// ```no_run
    /// use mk_geolocation::callback::ContinuousPosition;
    ///
    /// let continuous_position = ContinuousPosition::new(move |position: mk_geolocation::GeolocationPosition| {
    ///     // use position...
    /// });
    ///
    /// // If position shouldn't be watched, then cancel it.
    /// if nevermind() {
    ///     continuous_position.cancel();
    /// }
    /// # fn nevermind() -> bool { true }
    /// ```
    pub fn cancel(
        mut self,
    ) -> (
        Closure<dyn FnMut(GeolocationPosition)>,
        Option<Closure<dyn FnMut(GeolocationPositionError)>>,
    ) {
        let success = self.success_closure.take().unwrap_throw();
        let error = self.error_closure.take();
        (success, error)
    }

    /// Clear the continuous position request based on the previously returned ID.
    ///
    /// # Example
    ///
    /// ```no_run
    /// use mk_geolocation::callback::ContinuousPosition;
    ///
    /// // We definitely want to get and use a position, and aren't going to ever cancel this
    /// // request.
    /// let watch_id = ContinuousPosition::new(move |position| {
    ///     // use position...
    /// }).forget();
    ///
    /// // do something else
    ///
    /// // clear the continuous position request
    /// ContinuousPosition::clear_watch(watch_id);
    /// ```
    pub fn clear_watch(id: i32) {
        GLOBAL.with(|global| global.geolocation.clear_watch(id));
    }
}
