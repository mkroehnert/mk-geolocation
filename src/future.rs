//! `Future`- and `Stream`-backed geolocation APIs.

use crate::callback::{ContinuousPosition, Position};

use futures_channel::{mpsc, oneshot};
use futures_core::stream::Stream;
use std::future::Future;
use std::pin::Pin;
use std::task::{Context, Poll};
use wasm_bindgen::prelude::*;

use crate::geolocation::{GeolocationPosition, GeolocationPositionError, PositionOptions};

pub type GeolocationPositionResult = Result<GeolocationPosition, GeolocationPositionError>;

/// A position request as a `Future`.
///
/// See `PositionFuture::new` for scheduling new current positions.
///
/// Once scheduled, if you change your mind and don't want need the position,
/// you can `drop` the future.
///
/// A position future will never resolve to `Err`. Its only failure mode is when
/// retrieving the position takes so long that it is effectively infinite and never fires.
///
/// # Example
///
/// ```no_run
/// use mk_geolocation::future::PositionFuture;
/// use wasm_bindgen_futures::spawn_local;
///
/// spawn_local(async {
///     PositionFuture::new().await.and_then(|result| {
///         result.and_then(|position| {
///             // use position...
///         }).or_else(|err|{ /* handle error */ });
///     })
/// });
/// ```

#[derive(Debug)]
#[must_use = "futures do nothing unless polled or spawned"]
pub struct PositionFuture {
    inner: Position,
    rx: oneshot::Receiver<GeolocationPosition>,
    rx_err: oneshot::Receiver<GeolocationPositionError>,
}

impl PositionFuture {
    /// Create a new position future.
    ///
    /// Remember that futures do nothing unless polled or spawned, so either
    /// pass this future to `wasm_bindgen_futures::spawn_local` or use it inside
    /// another future.
    ///
    /// # Example
    ///
    /// ```no_run
    /// use mk_geolocation::future::PositionFuture;
    /// use wasm_bindgen_futures::spawn_local;
    ///
    /// spawn_local(async {
    ///     let pos_result = PositionFuture::new().await;
    ///     // Do something with pos_result ...
    /// });
    /// ```
    pub fn new() -> Self {
        let options = PositionOptions::new();
        Self::new_with_options(options)
    }

    pub fn new_with_options(options: PositionOptions) -> Self {
        let (tx, rx) = oneshot::channel();
        let (tx_err, rx_err) = oneshot::channel();

        let inner = Position::new_with_error_callback_and_options(
            move |position: GeolocationPosition| {
                // do nothing if the receiver was dropped
                tx.send(position).unwrap_throw();
            },
            move |position_error: GeolocationPositionError| {
                // do nothing if the receiver was dropped
                tx_err.send(position_error).unwrap_throw();
            },
            options,
        );
        Self { inner, rx, rx_err }
    }
}

impl Future for PositionFuture {
    type Output = GeolocationPositionResult;

    fn poll(mut self: Pin<&mut Self>, ctx: &mut Context) -> Poll<Self::Output> {
        //Future::poll(Pin::new(&mut self.rx), ctx).map(|t| Ok(t.unwrap_throw()))
        match Future::poll(Pin::new(&mut self.rx), ctx) {
            Poll::Ready(pos) => Poll::Ready(Ok::<GeolocationPosition, GeolocationPositionError>(
                pos.unwrap_throw(),
            )),
            Poll::Pending => match Future::poll(Pin::new(&mut self.rx_err), ctx) {
                Poll::Ready(pos_err) => Poll::Ready(Err::<
                    GeolocationPosition,
                    GeolocationPositionError,
                >(pos_err.unwrap_throw())),
                Poll::Pending => Poll::Pending,
            },
        }
    }
}

/// A scheduled position as a `Stream`.
///
/// See `PostionStream::new` for scheduling new positions.
///
/// Once scheduled, if you want to stop the position from continuously triggering,
/// you can `drop` the stream.
///
/// A position stream will resolve to `Err`, if the maximum timeout has passed.

#[derive(Debug)]
#[must_use = "streams do nothing unless polled or spawned"]
pub struct PositionStream {
    inner: ContinuousPosition,
    receiver: mpsc::UnboundedReceiver<GeolocationPositionResult>,
}

impl PositionStream {
    /// Create a new continuous position stream.
    ///
    /// Remember that streams do nothing unless polled or spawned, so either
    /// spawn this stream via `wasm_bindgen_futures::spawn_local` or use it inside
    /// another stream or future.
    ///
    /// # Example
    ///
    /// ```no_run
    ///
    /// use futures_util::stream::StreamExt;
    /// use futures_util::future::ready;
    /// use mk_geolocation::future::PositionStream;
    /// use wasm_bindgen_futures::spawn_local;
    ///
    /// spawn_local(async {
    ///     PositionStream::new().for_each(|p: PositionStreamResult| {
    ///         // Do stuff with the new position or error ...
    ///         ready(())
    ///     }).await;
    /// });
    /// ```
    pub fn new() -> Self {
        let options = PositionOptions::new();
        Self::new_with_options(options)
    }

    pub fn new_with_options(options: PositionOptions) -> Self {
        let (sender, receiver) = mpsc::unbounded();
        let err_sender = sender.clone();
        let inner = ContinuousPosition::new_with_error_callback_and_options(
            move |position: GeolocationPosition| {
                // don't do anything if the receiver was dropped
                sender.unbounded_send(Ok(position)).unwrap_throw();
            },
            move |position_err: GeolocationPositionError| {
                // don't do anything if the receiver was dropped
                err_sender.unbounded_send(Err(position_err)).unwrap_throw();
            },
            options,
        );

        Self { inner, receiver }
    }

    pub fn cancel(self) {
        self.inner.cancel();
    }
}

impl Stream for PositionStream {
    type Item = GeolocationPositionResult;

    fn poll_next(mut self: Pin<&mut Self>, cx: &mut Context) -> Poll<Option<Self::Item>> {
        Stream::poll_next(Pin::new(&mut self.receiver), cx)
    }
}
