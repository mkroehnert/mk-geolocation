/*

Working with the geolocation API on the Web:

These APIs come in two flavors:
1. a callback style (that more directly mimics the JavaScript APIs), and
2. a `Future`s and `Stream`s API.

## Geolocation


### Geolocation with a Callback Function
 */

#![cfg_attr(feature = "futures", doc = "```no_run")]
#![cfg_attr(not(feature = "futures"), doc = "```ignore")]
/*
### Geolocation as `Future`s

With the `futures` feature enabled, a `future` module is exposed
which contains a futures-based geolocation API.

*/

// TODO enable once all is documented
// #![deny(missing_docs, missing_debug_implementations)]

mod geolocation;
pub use geolocation::{
    GeolocationCoordinates, GeolocationPosition, GeolocationPositionError, PositionOptions,
};

pub mod callback;

#[cfg(feature = "futures")]
pub mod future;
