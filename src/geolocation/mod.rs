/// export generated WASM bindings
/// taken from https://github.com/rustwasm/wasm-bindgen/pull/2578

#[allow(non_snake_case)]
mod gen_Navigator;
pub use gen_Navigator::*;

#[allow(non_snake_case)]
mod gen_Geolocation;
pub use gen_Geolocation::*;

#[allow(non_snake_case)]
mod gen_GeolocationCoordinates;
pub use gen_GeolocationCoordinates::*;

#[allow(non_snake_case)]
mod gen_GeolocationPositionError;
pub use gen_GeolocationPositionError::*;

#[allow(non_snake_case)]
mod gen_GeolocationPosition;
pub use gen_GeolocationPosition::*;

#[allow(non_snake_case)]
mod gen_PositionOptions;
pub use gen_PositionOptions::*;
