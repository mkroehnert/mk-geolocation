#![allow(unused_imports)]
use super::*;
use wasm_bindgen::prelude::*;
#[wasm_bindgen]
extern "C" {
    /*
        # [wasm_bindgen (extends = :: js_sys :: Object , js_name = Navigator , typescript_type = "Navigator")]
        #[derive(Debug, Clone, PartialEq, Eq)]
        #[doc = "The `Navigator` class."]
        #[doc = ""]
        #[doc = "[MDN Documentation](https://developer.mozilla.org/en-US/docs/Web/API/Navigator)"]
        #[doc = ""]
        #[doc = "*This API requires the following crate features to be activated: `Navigator`*"]
    */
    pub type Navigator;

    //#[cfg(feature = "Geolocation")]
    # [wasm_bindgen (structural , catch , method , getter , js_class = "Navigator" , js_name = geolocation)]
    #[doc = "Getter for the `geolocation` field of this object."]
    #[doc = ""]
    #[doc = "[MDN Documentation](https://developer.mozilla.org/en-US/docs/Web/API/Navigator/geolocation)"]
    #[doc = ""]
    #[doc = "*This API requires the following crate features to be activated: `Geolocation`, `Navigator`*"]
    pub fn geolocation(this: &Navigator) -> Result<Geolocation, JsValue>;
}
