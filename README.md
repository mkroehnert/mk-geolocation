<div align="center">

  <h1><code>mk-geolocation</code></h1>

</div>


Working with geolocation on the web.

These APIs come in two flavors:

1. a callback style (that more directly mimics the JavaScript APIs), and
2. a `Future`s and `Stream`s API.

#### WARNING

* initial structure taken from other gloo-* crates
* implementation not fully finished
  *  callback style API is working for me
  * futures style API is currently working only for single position requests
* resource handling/cleanup most likely needs some cleanup
* documentation is partially available but also needs additions/cleanup
* due to outdated web_sys interfaces for current browsers, updated generated wasm-bindgen bindings are included and exported in this crate (taken from https://github.com/rustwasm/wasm-bindgen/pull/2578)

#### Single Position with a Callback Function

```rust
use mk_geolocation::callback::Position;
use mk_geolocation::GeolocationPosition;

let position = Position::new(move |pos: GeolocationPosition| {
    // do something with the returned position
});

// Since we don't plan on cancelling the position request, call `forget`.
position.forget();
```

#### Continuous Position with a Callback Function

```rust
use mk_geolocation::callback::ContinuousPosition;
use mk_geolocation::GeolocationPosition;

let continuous_position = ContinuousPosition::new(move |pos: GeolocationPosition| {
    // do something with the returned position
});

// Since we don't plan on cancelling the position request, call `forget`.
continuous_position.forget();

// wait some time or do something
ContinuousPosition::clear_watch(*watch_id)

```

#### Single Position as `Future`

With the `futures` feature enabled, a `future` module containing futures-based
position requests is exposed.

#### Continuous Positions as `Future`s

With the `futures` feature enabled, a `future` module containing futures-based
position requests is exposed.
